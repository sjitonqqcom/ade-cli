# Copyright 2018  Ternaris.
# SPDX-License-Identifier: Apache-2.0

"""Store and retrieve credentials for docker and GitLab or DockerHub APIs."""

import json
import os
import sys
from collections import OrderedDict

import click

from .utils import find_file


CFG = None
FILENAME = '.adecreds'


def dockerhub_prompt():
    print("""
Create access token:

  https://hub.docker.com/settings/security/
""")


def gitlab_prompt(gitlab_host):
    print("""
Create access token:

  https://{}/profile/personal_access_tokens

with scopes:

  read_registry
  api

API scope is necessary to select tags.
""".lstrip().format(gitlab_host))


def open_credentials_file(filename):
    """Open credentials file, creating it with mode 0o600 if missing."""
    adehome = find_file('.adehome', parent=True)
    if adehome is None:
        print('ERROR: Not within an ade home!', file=sys.stderr)
        sys.exit(2)

    credsfile = adehome / filename
    try:
        f = open(credsfile, 'x+')
        os.chmod(credsfile, 0o600)
    except FileExistsError:
        f = open(credsfile, 'r+')
    f.close()
    return credsfile


def get_credentials(host, reset=False):
    """Return username and token for host."""
    credsfile = find_file(FILENAME)
    if credsfile is None:
        credsfile = open_credentials_file(FILENAME)

    try:
        raw = credsfile.read_text()
        allcreds = json.loads(raw) if raw else {}
    except json.decoder.JSONDecodeError:
        print('ERROR: {} is not valid json!'.format(credsfile), file=sys.stderr)
        click.confirm('Do you want to wipe and recreate it', abort=True)
        allcreds = {}

    try:
        creds = allcreds.setdefault(host, {})
        if reset:
            raise KeyError
        username = creds['username']
        token = creds['token']
    except KeyError:
        print('Please provide credentials for', host)
        username = creds['username'] = click.prompt('Username', default=creds.get('username'))
        if host == CFG.registry:
            if CFG.gitlab:
                gitlab_prompt(CFG.gitlab)
            elif CFG.dockerhub:
                dockerhub_prompt()
            else:
                print("""
Unknown Docker registry, please specify either ADE_GITLAB or ADE_DOCKERHUB environment
variables.
""".lstrip())
        token = creds['token'] = click.prompt('Token', default=creds.get('token'))

    credsfile.write_text(json.dumps(allcreds, sort_keys=True, indent=2))

    return OrderedDict((
        ('username', username),
        ('token', token),
    ))
